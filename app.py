from flask import Flask, current_app, jsonify, make_response, request, Blueprint
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from datetime import datetime, timedelta
import polars as pl
from cache import ttl_lru_cache
import logging

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)
db = SQLAlchemy()
api = Blueprint('api', __name__, url_prefix='/')


class Counts(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.DateTime)

    def __init__(self):
        self.datetime = datetime.now()


@api.route('/inc', methods=['PUT'])
def increment_count():
    if current_app.config['API_KEY'] != request.headers.get('X-API-KEY'):
        current_app.logger.error(f'[{datetime.now()}]: auth failed')
        return make_response('Unauthorized', 401)
    else:
        count = Counts()
        db.session.add(count)
        db.session.commit()
        current_app.logger.info(f'[{datetime.now()}]: incremented count by 1')
        return 'OK'


@api.route('/count', methods=['GET'])
@ttl_lru_cache(seconds_to_live=60*60)
def get_count():
    counts = pl.read_sql('SELECT * FROM counts', current_app.config['CON_X_DATABASE_URI'])
    counts = counts.groupby_dynamic("datetime", every="1d").agg(pl.col("id").count())
    counts = counts.rename({'id': 'count'})
    three_weeks_ago = datetime.now() - timedelta(days=7*3)
    counts = counts[counts.datetime > three_weeks_ago]
    return jsonify(counts.to_dict(as_series=False))


def create_app():
    app = Flask(__name__)
    app.config.from_prefixed_env()

    db.init_app(app)
    db.create_all(app=app)
    CORS(app)

    app.register_blueprint(api)

    return app
