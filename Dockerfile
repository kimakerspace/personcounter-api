FROM python:3.10 AS compile-image

## Virtualenv
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

## Add and install requirements
RUN pip install --upgrade pip 
COPY ./requirements.txt requirements.txt
RUN pip install -r requirements.txt

## runtime-image
FROM python:3.10 AS runtime-image

## Virtualenv
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

## Copy Python dependencies from build image
COPY --from=compile-image /opt/venv /opt/venv

## set working directory
WORKDIR /usr/src/app

## Add webserver and librays
COPY ./app.py /usr/src/app/app.py
COPY ./cache.py /usr/src/app/cache.py

## Switch to non-root user
# for some reason doing this before the copy results in weird permissions # && chmod -R 775 /usr/src/app/
# RUN chown -R tcp:tcp /usr/src/app/
RUN addgroup --system --gid 420 tcp && adduser --system --no-create-home --uid 420 --gid 420 tcp
USER tcp

EXPOSE 5000

## Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV PATH="/opt/venv/bin:$PATH"
ENV FLASK_APP="app:create_app"


CMD ["gunicorn","-b 0.0.0.0:5000", "app:create_app()"]
# CMD ["flask", "run", "--port", "5000", "--host", "0.0.0.0"]
